'use strict';

const imagesArr = [...document.querySelectorAll('.image-to-show')];
const buttonStop = document.querySelector('.button')

let currentImg = 0;
let previousImg = 0;

function start() {
    buttonStop.classList.add('active');
    buttonStop.addEventListener('click', () => {
        clearInterval(timerInterval);
        buttonStop.remove();
    })

    let timerInterval = setInterval(() => {
        imagesArr[previousImg].classList.remove('active');
        currentImg++;
        if (currentImg > imagesArr.length - 1) currentImg = 0;
        imagesArr[currentImg].classList.add('active');
        previousImg = currentImg;
    }, 3050)
}

start();

